﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace pikselal
{
    public partial class griFr : Form
    {
        Bitmap kaynak, islem;

        public griFr()
        {
            InitializeComponent();
        }

        private void dosyaToolStripMenuItem_Click(object sender, EventArgs e)
        {


        }

        private void griFr_Load(object sender, EventArgs e)
        {

        }

        private void açToolStripMenuItem_Click(object sender, EventArgs e)
        {

            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                kaynak = new Bitmap(openFileDialog1.FileName);
                kaynakBox.Image = kaynak;
            }
        }

        private void ortalamaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width;
            int yuk = kaynak.Height;
            islem = new Bitmap(gen, yuk);

            for (int x = 0; x < gen; x++) 
            {
                for (int y = 0; y < yuk; y++) 
                {
                    Color renliPiksel = kaynak.GetPixel(x, y);
                    int grideğer = (renliPiksel.R + renliPiksel.G + renliPiksel.B) / 3;
                    Color griPiksel = Color.FromArgb(grideğer, grideğer, grideğer);
                    islem.SetPixel(x, y, griPiksel);
                }
            }
            islemBox.Image = islem;



        }

        private void bT709ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width;
            int yuk = kaynak.Height;
            islem = new Bitmap(gen, yuk);

            for (int x = 0; x < gen; x++)
            {
                for (int y = 0; y < yuk; y++)
                {
                    Color renliPiksel = kaynak.GetPixel(x, y);
                   double gri = (renliPiksel.R * 0.2125 + renliPiksel.G * 0.7154 + renliPiksel.B * 0.072);
                   int grideğer = Convert.ToInt32(gri);
                    Color griPiksel = Color.FromArgb(grideğer, grideğer, grideğer);
                    islem.SetPixel(x, y, griPiksel);
                }
            }
            islemBox.Image = islem;

        }

        private void lumaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width;
            int yuk = kaynak.Height;
            islem = new Bitmap(gen, yuk);

            for (int x = 0; x < gen; x++)
            {
                for (int y = 0; y < yuk; y++)
                {
                    Color renliPiksel = kaynak.GetPixel(x, y);
                    double gri = (renliPiksel.R * 0.3 + renliPiksel.G * 0.59 + renliPiksel.B * 0.11);
                    int grideğer = Convert.ToInt32(gri);
                    Color griPiksel = Color.FromArgb(grideğer, grideğer, grideğer);
                    islem.SetPixel(x, y, griPiksel);
                }
            }
            islemBox.Image = islem;
        }

        private void açıklıkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width;
            int yuk = kaynak.Height;
            islem = new Bitmap(gen, yuk);

            for (int x = 0; x < gen; x++)
            {
                for (int y = 0; y < yuk; y++)
                {
                    Color renliPiksel = kaynak.GetPixel(x, y);
                    int max = 0, min = 0;
                    int a = renliPiksel.R;
                    int b = renliPiksel.G;
                    int c = renliPiksel.B;
                    int[] sayılar = new int[] { a, b, c };
                    int Max = sayılar[0];
                    for (int i = 0; i < sayılar.Length; i++)
                    {
                        if (Max < sayılar[i])
                        { Max = sayılar[i]; }
                    }
                    int Min = sayılar[0];
                    for (int i = 0; i < sayılar.Length; i++)
                    {
                        if (Min < sayılar[i])
                        { Min = sayılar[i]; }
                    }
                    int grideğer = (Max + Min) / 2;
                    Color griPiksel = Color.FromArgb(grideğer, grideğer, grideğer);
                    islem.SetPixel(x, y, griPiksel);
                }
                islemBox.Image = islem;
            }

                    
        }

        private void renkKanalıToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width;
            int yuk = kaynak.Height;
            islem = new Bitmap(gen, yuk);

            for (int x = 0; x < gen; x++)
            {
                for (int y = 0; y < yuk; y++)
                {
                    Color renliPiksel = kaynak.GetPixel(x, y);
                    int grideğer = renliPiksel.B;
                    Color griPiksel = Color.FromArgb(grideğer, grideğer, grideğer);
                    islem.SetPixel(x, y, griPiksel);
                }
            }
            islemBox.Image = islem;
        }

        private void renkKanalıRedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width;
            int yuk = kaynak.Height;
            islem = new Bitmap(gen, yuk);

            for (int x = 0; x < gen; x++)
            {
                for (int y = 0; y < yuk; y++)
                {
                    Color renliPiksel = kaynak.GetPixel(x, y);
                    int grideğer = renliPiksel.R;
                    Color griPiksel = Color.FromArgb(grideğer, grideğer, grideğer);
                    islem.SetPixel(x, y, griPiksel);
                }
            }
            islemBox.Image = islem;
        }

        private void renkKanalıGreenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width;
            int yuk = kaynak.Height;
            islem = new Bitmap(gen, yuk);

            for (int x = 0; x < gen; x++)
            {
                for (int y = 0; y < yuk; y++)
                {
                    Color renliPiksel = kaynak.GetPixel(x, y);
                    int grideğer = renliPiksel.G;
                    Color griPiksel = Color.FromArgb(grideğer, grideğer, grideğer);
                    islem.SetPixel(x, y, griPiksel);
                }
            }
            islemBox.Image = islem;
        }

        private void normalizeRedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width;
            int yuk = kaynak.Height;
            islem = new Bitmap(gen, yuk);

            for (int x = 0; x < gen; x++)
            {
                for (int y = 0; y < yuk; y++)
                {
                    Color renliPiksel = kaynak.GetPixel(x, y);
                    int grideğer = (255*renliPiksel.R) / (renliPiksel.R + renliPiksel.G + renliPiksel.B);
                    Color griPiksel = Color.FromArgb(grideğer, grideğer, grideğer);
                    islem.SetPixel(x, y, griPiksel);
                }
            }
            islemBox.Image = islem;
        }

        private void normalizeGreenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width;
            int yuk = kaynak.Height;
            islem = new Bitmap(gen, yuk);

            for (int x = 0; x < gen; x++)
            {
                for (int y = 0; y < yuk; y++)
                {
                    Color renliPiksel = kaynak.GetPixel(x, y);
                    int grideğer = (255 * renliPiksel.G) / (renliPiksel.R + renliPiksel.G + renliPiksel.B);
                    Color griPiksel = Color.FromArgb(grideğer, grideğer, grideğer);
                    islem.SetPixel(x, y, griPiksel);
                }
            }
            islemBox.Image = islem;
        }

        private void normalizeBlueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int gen = kaynak.Width;
            int yuk = kaynak.Height;
            islem = new Bitmap(gen, yuk);

            for (int x = 0; x < gen; x++)
            {
                for (int y = 0; y < yuk; y++)
                {
                    Color renliPiksel = kaynak.GetPixel(x, y);
                    int grideğer = (255 * renliPiksel.B) / (renliPiksel.R + renliPiksel.G + renliPiksel.B);
                    Color griPiksel = Color.FromArgb(grideğer, grideğer, grideğer);
                    islem.SetPixel(x, y, griPiksel);
                }
            }
            islemBox.Image = islem;
        }
    }
}
