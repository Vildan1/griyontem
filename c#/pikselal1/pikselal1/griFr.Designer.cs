﻿namespace pikselal
{
    partial class griFr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dosyaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.açToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.griYöntemleriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ortalamaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bT709ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lumaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.açıklıkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renkKanalıToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.islemBox = new System.Windows.Forms.PictureBox();
            this.kaynakBox = new System.Windows.Forms.PictureBox();
            this.renkKanalıRedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renkKanalıGreenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.normalizeRedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.normalizeGreenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.normalizeBlueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.islemBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kaynakBox)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dosyaToolStripMenuItem,
            this.griYöntemleriToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1503, 33);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // dosyaToolStripMenuItem
            // 
            this.dosyaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.açToolStripMenuItem});
            this.dosyaToolStripMenuItem.Name = "dosyaToolStripMenuItem";
            this.dosyaToolStripMenuItem.Size = new System.Drawing.Size(74, 29);
            this.dosyaToolStripMenuItem.Text = "Dosya";
            this.dosyaToolStripMenuItem.Click += new System.EventHandler(this.dosyaToolStripMenuItem_Click);
            // 
            // açToolStripMenuItem
            // 
            this.açToolStripMenuItem.Name = "açToolStripMenuItem";
            this.açToolStripMenuItem.Size = new System.Drawing.Size(104, 30);
            this.açToolStripMenuItem.Text = "Aç";
            this.açToolStripMenuItem.Click += new System.EventHandler(this.açToolStripMenuItem_Click);
            // 
            // griYöntemleriToolStripMenuItem
            // 
            this.griYöntemleriToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ortalamaToolStripMenuItem,
            this.bT709ToolStripMenuItem,
            this.lumaToolStripMenuItem,
            this.açıklıkToolStripMenuItem,
            this.renkKanalıToolStripMenuItem,
            this.renkKanalıRedToolStripMenuItem,
            this.renkKanalıGreenToolStripMenuItem,
            this.normalizeRedToolStripMenuItem,
            this.normalizeGreenToolStripMenuItem,
            this.normalizeBlueToolStripMenuItem});
            this.griYöntemleriToolStripMenuItem.Name = "griYöntemleriToolStripMenuItem";
            this.griYöntemleriToolStripMenuItem.Size = new System.Drawing.Size(135, 29);
            this.griYöntemleriToolStripMenuItem.Text = "Gri yöntemleri";
            // 
            // ortalamaToolStripMenuItem
            // 
            this.ortalamaToolStripMenuItem.Name = "ortalamaToolStripMenuItem";
            this.ortalamaToolStripMenuItem.Size = new System.Drawing.Size(210, 30);
            this.ortalamaToolStripMenuItem.Text = "Ortalama";
            this.ortalamaToolStripMenuItem.Click += new System.EventHandler(this.ortalamaToolStripMenuItem_Click);
            // 
            // bT709ToolStripMenuItem
            // 
            this.bT709ToolStripMenuItem.Name = "bT709ToolStripMenuItem";
            this.bT709ToolStripMenuItem.Size = new System.Drawing.Size(210, 30);
            this.bT709ToolStripMenuItem.Text = "BT 709";
            this.bT709ToolStripMenuItem.Click += new System.EventHandler(this.bT709ToolStripMenuItem_Click);
            // 
            // lumaToolStripMenuItem
            // 
            this.lumaToolStripMenuItem.Name = "lumaToolStripMenuItem";
            this.lumaToolStripMenuItem.Size = new System.Drawing.Size(210, 30);
            this.lumaToolStripMenuItem.Text = "Luma";
            this.lumaToolStripMenuItem.Click += new System.EventHandler(this.lumaToolStripMenuItem_Click);
            // 
            // açıklıkToolStripMenuItem
            // 
            this.açıklıkToolStripMenuItem.Name = "açıklıkToolStripMenuItem";
            this.açıklıkToolStripMenuItem.Size = new System.Drawing.Size(210, 30);
            this.açıklıkToolStripMenuItem.Text = "Açıklık";
            this.açıklıkToolStripMenuItem.Click += new System.EventHandler(this.açıklıkToolStripMenuItem_Click);
            // 
            // renkKanalıToolStripMenuItem
            // 
            this.renkKanalıToolStripMenuItem.Name = "renkKanalıToolStripMenuItem";
            this.renkKanalıToolStripMenuItem.Size = new System.Drawing.Size(210, 30);
            this.renkKanalıToolStripMenuItem.Text = "Renk kanalı Blue";
            this.renkKanalıToolStripMenuItem.Click += new System.EventHandler(this.renkKanalıToolStripMenuItem_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // islemBox
            // 
            this.islemBox.Location = new System.Drawing.Point(581, 114);
            this.islemBox.Name = "islemBox";
            this.islemBox.Size = new System.Drawing.Size(460, 480);
            this.islemBox.TabIndex = 4;
            this.islemBox.TabStop = false;
            // 
            // kaynakBox
            // 
            this.kaynakBox.Location = new System.Drawing.Point(61, 114);
            this.kaynakBox.Name = "kaynakBox";
            this.kaynakBox.Size = new System.Drawing.Size(460, 480);
            this.kaynakBox.TabIndex = 3;
            this.kaynakBox.TabStop = false;
            // 
            // renkKanalıRedToolStripMenuItem
            // 
            this.renkKanalıRedToolStripMenuItem.Name = "renkKanalıRedToolStripMenuItem";
            this.renkKanalıRedToolStripMenuItem.Size = new System.Drawing.Size(210, 30);
            this.renkKanalıRedToolStripMenuItem.Text = "Renk kanalı Red";
            this.renkKanalıRedToolStripMenuItem.Click += new System.EventHandler(this.renkKanalıRedToolStripMenuItem_Click);
            // 
            // renkKanalıGreenToolStripMenuItem
            // 
            this.renkKanalıGreenToolStripMenuItem.Name = "renkKanalıGreenToolStripMenuItem";
            this.renkKanalıGreenToolStripMenuItem.Size = new System.Drawing.Size(223, 30);
            this.renkKanalıGreenToolStripMenuItem.Text = "Renk kanalı Green";
            this.renkKanalıGreenToolStripMenuItem.Click += new System.EventHandler(this.renkKanalıGreenToolStripMenuItem_Click);
            // 
            // normalizeRedToolStripMenuItem
            // 
            this.normalizeRedToolStripMenuItem.Name = "normalizeRedToolStripMenuItem";
            this.normalizeRedToolStripMenuItem.Size = new System.Drawing.Size(223, 30);
            this.normalizeRedToolStripMenuItem.Text = "Normalize Red";
            this.normalizeRedToolStripMenuItem.Click += new System.EventHandler(this.normalizeRedToolStripMenuItem_Click);
            // 
            // normalizeGreenToolStripMenuItem
            // 
            this.normalizeGreenToolStripMenuItem.Name = "normalizeGreenToolStripMenuItem";
            this.normalizeGreenToolStripMenuItem.Size = new System.Drawing.Size(223, 30);
            this.normalizeGreenToolStripMenuItem.Text = "Normalize Green";
            this.normalizeGreenToolStripMenuItem.Click += new System.EventHandler(this.normalizeGreenToolStripMenuItem_Click);
            // 
            // normalizeBlueToolStripMenuItem
            // 
            this.normalizeBlueToolStripMenuItem.Name = "normalizeBlueToolStripMenuItem";
            this.normalizeBlueToolStripMenuItem.Size = new System.Drawing.Size(223, 30);
            this.normalizeBlueToolStripMenuItem.Text = "Normalize Blue";
            this.normalizeBlueToolStripMenuItem.Click += new System.EventHandler(this.normalizeBlueToolStripMenuItem_Click);
            // 
            // griFr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1503, 739);
            this.Controls.Add(this.islemBox);
            this.Controls.Add(this.kaynakBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "griFr";
            this.Text = "griFr";
            this.Load += new System.EventHandler(this.griFr_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.islemBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kaynakBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem dosyaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem açToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem griYöntemleriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ortalamaToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.PictureBox islemBox;
        private System.Windows.Forms.PictureBox kaynakBox;
        private System.Windows.Forms.ToolStripMenuItem bT709ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lumaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem açıklıkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renkKanalıToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renkKanalıRedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renkKanalıGreenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem normalizeRedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem normalizeGreenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem normalizeBlueToolStripMenuItem;
    }
}